/// Active user interface
#[derive(Debug)]
pub enum Ui {
    Gui,
    Tui,
}

/// Graphical user interface
#[cfg(feature = "gui")]
pub const UI: Ui = Ui::Gui;

/// Text user interface
#[cfg(feature = "tui")]
pub const UI: Ui = Ui::Tui;
